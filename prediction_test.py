from sklearn import linear_model
from sklearn.model_selection import train_test_split

from dags.db_operations import get_data_frame
from sklearn.preprocessing import LabelEncoder


def get_accuracy_score():
    dataframe = get_data_frame()
    x = dataframe[['YEAR', 'MONTH', 'DAY', 'HOUR']].copy(deep=False)
    x['DIRECTION'] = LabelEncoder().fit_transform(dataframe['DIRECTION'])
    y = dataframe[['AMOUNT', 'COUNT']]
    x_train, x_test, y_train, y_test = train_test_split(x.values, y.values, test_size=0.2)
    model = linear_model.LinearRegression()
    model.fit(x_train, y_train)
    return model.score(x_test, y_test)
