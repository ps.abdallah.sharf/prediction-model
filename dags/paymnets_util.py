from db_operations import get_payments_details
from model import PaymentDatesTypes, PaymentCountAmount


def get_payments_per_hour_per_day():
    payments_per_hor_per_day_map = {}
    for val in get_payments_details():
        payment_date_type = PaymentDatesTypes(val[0].year, val[0].month, val[0].day, val[0].hour, val[2])
        if payment_date_type in payments_per_hor_per_day_map:
            payments = payments_per_hor_per_day_map[payment_date_type]
            payments_per_hor_per_day_map[payment_date_type] = PaymentCountAmount(payments.count + 1,
                                                                                 payments.total + val[1])
        else:
            payments_per_hor_per_day_map[payment_date_type] = PaymentCountAmount(1, val[1])
    return payments_per_hor_per_day_map
