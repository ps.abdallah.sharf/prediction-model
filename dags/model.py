class PaymentCountAmount:
    def __init__(self, count, total):
        self.count = count
        self.total = total

    def __str__(self):
        return 'Count: ' + str(self.count) + ' ' + \
               ',Total: ' + str(self.total)


class PaymentDatesTypes:
    def __init__(self, year, month, day, hour, direction):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.direction = direction

    def __key(self):
        return self.year, self.month, self.day, self.hour, self.direction

    def __hash__(self):
        return hash(self.__key())

    def __eq__(self, other):
        return isinstance(other, PaymentDatesTypes) \
               and self.__key() == other.__key()

    def __str__(self):
        return 'Year: ' + str(self.year) + ' ' + \
               ',month: ' + str(self.month) + ' ' + \
               ',day: ' + str(self.day) + ' ' + \
               ',hour: ' + str(self.hour) + ' ' + \
               ',direction: ' + str(self.direction)
