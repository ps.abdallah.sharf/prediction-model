import joblib
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeRegressor

from db_operations import get_data_frame

trained_graph_file_name = './dags/trained_graph.joblib'


def predict(input_values, model_file):
    return get_trained_prediction_model_from_file(model_file) \
        .predict([input_values])


def get_trained_prediction_model_from_file(model_file):
    return joblib.load(model_file)


def save_trained_graph_to_file():
    joblib.dump(get_prediction_model(), trained_graph_file_name)


def get_prediction_model():
    dataframe = get_data_frame()
    x = dataframe[['YEAR', 'MONTH', 'DAY', 'HOUR']].copy(deep=False)
    encoder = LabelEncoder()
    x['DIRECTION'] = encoder.fit_transform(dataframe['DIRECTION'])

    y = dataframe[['AMOUNT', 'COUNT']]

    model = DecisionTreeRegressor()

    model.fit(x.values, y)
    return model
