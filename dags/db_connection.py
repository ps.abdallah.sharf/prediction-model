import configparser
import cx_Oracle as Oracle
import psycopg2 as postgres
import pymssql


def get_db_connection():
    config = get_config()
    database_type = config.get('datasource', 'dbtype')
    if database_type == 'mssql':
        return get_sqlserver_connection(config)
    elif database_type == 'oracle10':
        return get_oracle_connection(config)
    elif database_type == 'postgres':
        return get_postgres_connection(config)


def get_config():
    config = configparser.RawConfigParser()
    config.read('./dags/config.properties')
    return config


def get_sqlserver_connection(config):
    return pymssql.connect(config.get('datasource', 'dns.name') + ':' + config.get('datasource', 'port'),
                           config.get('datasource', 'username'),
                           config.get('datasource', 'password'),
                           config.get('datasource', 'service.name'))


def get_oracle_connection(config):
    dsn_tns = Oracle.makedsn(config.get('datasource', 'dns.name'),
                             config.get('datasource', 'port'),
                             config.get('datasource', 'service.name'))

    return Oracle.connect(config.get('datasource', 'username'),
                          config.get('datasource', 'password'),
                          dsn_tns)


def get_postgres_connection(config):
    return postgres.connect(database=config.get('datasource', 'service.name'),
                            user=config.get('datasource', 'username'),
                            password=config.get('datasource', 'password'),
                            host=config.get('datasource', 'dns.name'),
                            port=config.get('datasource', 'port'))
