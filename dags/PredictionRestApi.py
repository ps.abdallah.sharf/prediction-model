from flask import Flask
from flask_restful import Api, Resource, reqparse

from prediction import predict

model_file = 'trained_graph.joblib'

app = Flask(__name__)
api = Api(app)


class PaymentPredication(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('year', required=True)
        parser.add_argument('month', required=True)
        parser.add_argument('day', required=True)
        parser.add_argument('hour', required=True)
        args = parser.parse_args()
        outward_payment = predict([args['year'], args['month'], args['day'], args['hour'], 1], model_file)
        inward_payment = predict([args['year'], args['month'], args['day'], args['hour'], 0], model_file)
        return {'outward_payment': dict(outward_payment),
                'inward_payment': dict(inward_payment)}, 200


api.add_resource(PaymentPredication, '/predicate-payments')

if __name__ == '__main__':
    app.run()
