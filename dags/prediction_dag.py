from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator

from db_operations import save_records, create_table
from paymnets_util import get_payments_per_hour_per_day
from prediction import save_trained_graph_to_file

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2022, 1, 5),
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

with DAG(
        dag_id='prediction-dag',
        default_args=default_args,
        description='Payment prediction DAG',
        schedule_interval=timedelta(days=1)
) as dag:
    create_table = PythonOperator(
        task_id="create_table",
        python_callable=create_table
    )

    insert_records = PythonOperator(
        task_id="insert_records",
        python_callable=save_records,
        op_kwargs={"fun": get_payments_per_hour_per_day}
    )

    create_model = PythonOperator(
        task_id="create_model",
        python_callable=save_trained_graph_to_file
    )

    create_table >> insert_records >> create_model
