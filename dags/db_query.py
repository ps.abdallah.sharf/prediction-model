sqlserver_create_table_if_not_exist_query = """if not exists (
select * from sysobjects where name='PAYMENT_PER_HOUR_PER_DAY')
create table PAYMENT_PER_HOUR_PER_DAY (
ID bigint NOT NULL IDENTITY(1,1),
YEAR int NOT NULL,
MONTH int NOT NULL,
DAY int NOT NULL,
HOUR int NOT NULL,
DIRECTION varchar(20) NOT NULL,
AMOUNT numeric(21, 8) NOT NULL,
COUNT int NOT NULL,
PRIMARY KEY (ID)
)"""

oracle_create_table_if_not_exist_query = """declare
begin
execute immediate 'create table "PAYMENT_PER_HOUR_PER_DAY" (
ID NUMBER(19, 0) NOT NULL GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
YEAR int NOT NULL,
MONTH int NOT NULL,
DAY int NOT NULL,
HOUR int NOT NULL,
DIRECTION varchar(20) NOT NULL,
AMOUNT NUMBER(19, 2) NOT NULL,
COUNT int NOT NULL,
PRIMARY KEY (ID)
)';
exception when others then
if SQLCODE = -955 then null; else raise; end if;
end;"""

postgres_create_table_if_not_exist_query = """CREATE TABLE IF NOT EXISTS payment_per_hour_per_day (
id BIGINT NOT NULL SERIAL,
year int NOT NULL,
month int NOT NULL,
day int NOT NULL,
hour int NOT NULL,
direction varchar(20) NOT NULL,
amount numeric(21, 8) NOT NULL,
count int NOT NULL,
PRIMARY KEY (id)
)"""

query_to_select_creation_datetime_and_amount_from_transaction_summary = """
SELECT Z_CREATION_DATE, TRANSFERAMOUNT, 
CASE TRANSACTIONTYPE WHEN 'CRT INWARD' then 'INWARD' WHEN 'Return INWARD' THEN 'INWARD' ELSE 'OUTWARD' END AS TRANSACTIONTYPE  FROM PPSTransactionSummary
"""

query_to_save_payment_per_hour_row = """
INSERT INTO PAYMENT_PER_HOUR_PER_DAY (YEAR, MONTH, DAY, HOUR, DIRECTION, AMOUNT, COUNT)
VALUES (%s, %s, %s, %s, %s, %s,%s);
"""

query_to_select_columns_from_payment_per_hour_table = """
SELECT *
FROM PAYMENT_PER_HOUR_PER_DAY;
"""
