from db_connection import get_config, get_db_connection


import pandas

from db_query import sqlserver_create_table_if_not_exist_query, oracle_create_table_if_not_exist_query, \
    postgres_create_table_if_not_exist_query, query_to_select_creation_datetime_and_amount_from_transaction_summary, \
    query_to_save_payment_per_hour_row, query_to_select_columns_from_payment_per_hour_table


def create_table():
    config = get_config()
    database_type = config.get('datasource', 'dbtype')
    if database_type == 'mssql':
        create_table_for_specific_database(sqlserver_create_table_if_not_exist_query)
    elif database_type == 'oracle10':
        create_table_for_specific_database(oracle_create_table_if_not_exist_query)
    elif database_type == 'postgres':
        create_table_for_specific_database(postgres_create_table_if_not_exist_query)


def create_table_for_specific_database(query):
    connection = get_db_connection()
    connection.cursor().execute(query)
    connection.commit()
    connection.close()


def get_payments_details():
    connection = get_db_connection()
    cursor = connection.cursor()
    cursor.execute(query_to_select_creation_datetime_and_amount_from_transaction_summary)
    data = cursor.fetchall()
    connection.close()
    return data


def save_records(fun):
    payment_map = fun()
    connection = get_db_connection()
    print(payment_map)
    print(type(payment_map))
    for key in payment_map:
        value = payment_map[key]
        connection.cursor() \
            .execute(query_to_save_payment_per_hour_row,
                     (key.year, key.month, key.day, key.hour, key.direction, value.total, value.count))
    connection.commit()
    connection.close()


def get_data_frame():
    connection = get_db_connection()
    df = pandas.read_sql_query(query_to_select_columns_from_payment_per_hour_table, connection)
    connection.close()
    return df
